import React from 'react';
import {View, StyleSheet, Text, Image, Dimensions} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {RootStackParamList} from 'App';

type MainScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Starter'
>;

type MainScreenRouteProp = RouteProp<RootStackParamList, 'Starter'>;

export interface Props {
  name: string;
  route: MainScreenRouteProp;
  navigation: MainScreenNavigationProp;
  enthusiasmLevel?: number;
}

const StarterScreen: React.FC<Props> = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Welcome to the React Native BoilerPlate Project by{' '}
        {props.route.params.param1}
      </Text>
      <Image
        style={styles.logo}
        source={require('../../../assets/inflightit_logo.png')}
      />
    </View>
  );
};

// styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
  },
  logo: {
    width: 300,
    height: 100,
    resizeMode: 'contain',
  },
});

export default StarterScreen;
