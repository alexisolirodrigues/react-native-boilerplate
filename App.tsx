import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import StarterScreen from './src/views/screens/SarterScreen';

export type RootStackParamList = {
  Starter: {param1: string};
};

const Stack = createStackNavigator<RootStackParamList>();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Starter"
          initialParams={{param1: ''}}
          component={StarterScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
