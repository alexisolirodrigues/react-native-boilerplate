### To clone this stater project to a folder on your local machine and use it, run the following commands:

#### Clone the project and setup the code base
    npm install
    cd ios && pod install
    react-native-rename <newName> -b <bundleIdentifier>
    
#### Change remotes
    git remote set-url <remote-name> <remote-url>
    
... and you're ready to go! :)